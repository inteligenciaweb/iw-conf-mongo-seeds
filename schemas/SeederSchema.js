const {Schema} = require('mongoose')

const seederSchema = new Schema({
  nome: String,
  dataHora: String
})

module.exports = seederSchema
