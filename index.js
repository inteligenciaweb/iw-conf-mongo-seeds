const SeedConfig = require('./configs/SeedConfig')
const iwSeeder = require('./schemas/SeederSchema')

const configs = {SeedConfig}
const schemas = {iwSeeder}

module.exports = {schemas, configs}
