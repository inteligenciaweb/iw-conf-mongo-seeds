const moment = require('moment')

module.exports = async ({seeds, models}) => {
  const allSeeds = Object.keys(seeds || {})
  const {iwSeeder} = models

  let execute = Promise.resolve()

  allSeeds.forEach(nome => (execute = execute.then(async () => {
    const seeder = await iwSeeder.findOne({nome})
    if (seeder) return
    const dataHora = moment().format('YYYY-MM-DD HH:mm:ss')
    await iwSeeder.create({nome, dataHora})
    return seeds[nome](models)
  })))

  return execute
}
